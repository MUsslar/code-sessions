﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Health
{
    public static PlayerHealth Instance;

    [SerializeField] private Image _healthBar;

    public override void ReduceHealth(int reduction)
    {
        base.ReduceHealth(reduction);

        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        _healthBar.fillAmount = CurrentHealth / (float) MaxHealth;
    }

    protected override void Start()
    {
        base.Start();
        
        UpdateHealthBar();
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }
}
