﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : Health
{
    [SerializeField] private Transform _healthBarContainer;
    [SerializeField] private Image _healthBar;

    public override void ReduceHealth(int reduction)
    {
        base.ReduceHealth(reduction);

        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        _healthBar.fillAmount = CurrentHealth / (float) MaxHealth;
    }

    private void Update()
    {
        _healthBarContainer.rotation = Quaternion.identity;
    }

    protected override void Start()
    {
        base.Start();

        UpdateHealthBar();
    }
}