using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    [SerializeField] private NavMeshAgent _agent;
    [Range(0, 100)] [SerializeField] private int _damage;
    [SerializeField] private EnemyHealth _health;

    public int Id { get; set; }

    public bool IsDead { get; private set; }

    public int TakeDamage(int damage)
    {
        _health.ReduceHealth(damage);

        if (_health.CurrentHealth <= 0)
        {
            IsDead = true;
            Destroy(gameObject);
        }

        return _health.CurrentHealth;
    }

    public void SetTarget(Vector3 targetPosition)
    {
        _agent.SetDestination(targetPosition);
    }

    public void Destroy()
    {
        PlayerHealth.Instance.ReduceHealth(_damage);
        Destroy(gameObject);
    }

    private void Awake()
    {
        IsDead = false;
    }
}
