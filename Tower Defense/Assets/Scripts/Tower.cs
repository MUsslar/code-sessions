﻿using System.Collections;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [Header("Laser")]
    [SerializeField] private LineRenderer _laserPrefab;
    [SerializeField] private Transform _cannonEnd;
    [SerializeField] private float _laserLifeTime;
    [Header("Shooting")]
    [SerializeField] private float _shootingInterval;
    [SerializeField] private int _damage;

    private Enemy _currentEnemy;

    private void Start()
    {
        StartCoroutine(TryShootEnemy());
    }

    private void Update()
    {
        if (_currentEnemy == null)
            return;

        var lookDirection = _currentEnemy.transform.position - gameObject.transform.position;
        lookDirection.y = 0;
        gameObject.transform.LookAt(gameObject.transform.position + lookDirection);
    }

    private IEnumerator TryShootEnemy()
    {
        while (true)
        {
            yield return new WaitForSeconds(_shootingInterval);

            TryShoot();
        }
    }

    private void TryShoot()
    {
        if (_currentEnemy == null)
            return;

        // deal damage
        var remainingHealth = _currentEnemy.TakeDamage(_damage);

        // particle effect
        var laser = Instantiate(_laserPrefab);
        laser.positionCount = 2;
        laser.SetPosition(0, _cannonEnd.position);
        laser.SetPosition(1, _currentEnemy.transform.position);
        Destroy(laser.gameObject, _laserLifeTime);

        // sound effect

        if (remainingHealth <= 0)
        {
            // if enemy health <= 0 -> _currentEnemy -> null
            _currentEnemy = null;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();

        if (enemy == null || _currentEnemy == null)
            return;

        if (_currentEnemy == enemy)
        {
            _currentEnemy = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        TryUpdateEnemy(other);
    }

    private void OnTriggerStay(Collider other)
    {
        TryUpdateEnemy(other);
    }

    private void TryUpdateEnemy(Component potentialEnemy)
    {
        Enemy newEnemy = potentialEnemy.GetComponent<Enemy>();

        if (newEnemy == null)
            return;

        if (IsFurtherEnemy(newEnemy))
        {
            _currentEnemy = newEnemy;
        }
    }

    private bool IsFurtherEnemy(Enemy newEnemy)
    {
        return _currentEnemy == null || newEnemy.Id < _currentEnemy.Id;
    }
}
