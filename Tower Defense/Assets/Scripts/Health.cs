﻿using System;
using UnityEngine;

public abstract class Health : MonoBehaviour
{
    [SerializeField] private int _maxHealth;

    public int CurrentHealth { get; private set; }

    protected int MaxHealth
    {
        get { return _maxHealth; }
    }

    public virtual void ReduceHealth(int reduction)
    {
        CurrentHealth -= reduction;
        CurrentHealth = Math.Max(CurrentHealth, 0);
    }

    protected virtual void Start()
    {
        CurrentHealth = _maxHealth;
    }
}