﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TowerSpawner : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject _towerPrefab;

    private bool _hasSpawned = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_hasSpawned)
            return;
        
        Instantiate(_towerPrefab, transform);
        _hasSpawned = true;
    }

    public void Help()
    {
        throw new System.NotImplementedException();
    }
}
