﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField] private Enemy _enemyPrefab;
    [SerializeField] private GameObject _spawnPoint;
    [SerializeField] private GameObject _targetPoint;
    [Space]
    [SerializeField] private float _spawnInterval;

    private int _id = 0;

    private void Start()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(_spawnInterval);

            var enemy = Instantiate(_enemyPrefab, _spawnPoint.transform.position, Quaternion.identity);
            enemy.SetTarget(_targetPoint.transform.position);

            enemy.Id = _id;
            _id += 1;
        }
    }
}
