﻿using UnityEngine;
using UnityEngine.UI;

public class DrawState : MonoBehaviour
{

    [SerializeField] private Button _drawButton;
    [SerializeField] private Animator _drawAnimator;

    private bool _isDrawing;

    public void StartDrawing()
    {
        _isDrawing = true;
        
        UpdateVisuals();
    }

    public void EndDrawing()
    {
        _isDrawing = false;

        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        UpdateAnimation();
        UpdateButton();
    }

    private void UpdateAnimation()
    {
        _drawAnimator.SetBool("IsBlending", _isDrawing);
    }

    private void UpdateButton()
    {
        _drawButton.interactable = !_isDrawing;
    }
}
