﻿using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour
{

    [SerializeField] private Image _card;
    [SerializeField] private Text _resultText;
    [SerializeField] private Text _currentChoiceText;

    private RedOrBlack.CardType _typeChoice;

    public void DisplayCard(RedOrBlack.CardType type)
    {
        _resultText.text = GetResultText(type);

        switch (type)
        {
            case RedOrBlack.CardType.Red:
                _card.color = Color.red;
                break;
            case RedOrBlack.CardType.Black:
                _card.color = Color.black;
                break;
            case RedOrBlack.CardType.None:
                _card.color = new Color(0, 0, 0, 0);
                break;
        }
    }

    public void InputChoice(int choice)
    {
        _typeChoice = (RedOrBlack.CardType) choice;

        _currentChoiceText.text = string.Format("Current choice: {0}", _typeChoice);
    }

    private string GetResultText(RedOrBlack.CardType type)
    {
        return _typeChoice == RedOrBlack.CardType.None
            ? "Please put in a choice."
            : _typeChoice == type
                ? "You won!"
                : "You lose.";
    }

    private void Start()
    {
        InputChoice(0);
    }
}
