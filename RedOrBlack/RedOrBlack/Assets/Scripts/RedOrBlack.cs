﻿using UnityEngine;

public class RedOrBlack : MonoBehaviour
{
    public enum CardType
    {
        None,
        Red,
        Black
    }

    [SerializeField] private CardDisplay _display;
    [SerializeField] private DrawState _drawState;

    private int _redCards;
    private int _blackCards;

    public void DrawCard()
    {
        if (_redCards == 0 || _blackCards == 0)
        {
            RefillDeck();
        }

        var chosenCard = Random.Range(0, _redCards + _blackCards);

        DisplayDrawnCard(chosenCard < _blackCards ? CardType.Red : CardType.Black);

        _drawState.StartDrawing();
    }

    public void RefillDeck()
    {
        _redCards = 32;
        _blackCards = 32;
    }

    private void DisplayDrawnCard(CardType type)
    {
        _display.DisplayCard(type);
    }
}
