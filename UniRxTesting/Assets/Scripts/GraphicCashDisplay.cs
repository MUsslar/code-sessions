﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GraphicCashDisplay : MonoBehaviour
{
    [SerializeField] private ReactiveTests _reactiveTests;
    [SerializeField] private Image _cashDisplay;

    private void Start()
    {
        _reactiveTests.IntegerStream.Subscribe(HandleInteger).AddTo(gameObject);
    }

    private void HandleInteger(int receivedInteger)
    {
        int cashBelow100 = receivedInteger % 100;
        _cashDisplay.fillAmount = cashBelow100 / 100.0f;
    }
}