﻿using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class ReactiveTests : MonoBehaviour
{
    private ReactiveProperty<int> _integerStream;

    [SerializeField] private Text _integerText;
    [SerializeField] private float _updateInterval;
    
    private int _idleGain = 1;
     
    public IReadOnlyReactiveProperty<int> IntegerStream
    {
        get { return _integerStream; }
    }

    public void IncreaseCash(int instantCashValue)
    {
        _integerStream.Value += instantCashValue;
    }

    public void UpdateIdleCash(int upgradeCost, int upgradeGain)
    {
        _idleGain += upgradeGain;
        _integerStream.Value -= upgradeCost;
    }

    private void Awake()
    {
        _integerStream = new ReactiveProperty<int>(0);
        
        _integerStream.Subscribe(HandleInteger).AddTo(gameObject);

        StartCoroutine(UpdateCash());
    }
    
    private IEnumerator UpdateCash()
    {
        while (true)
        {
            yield return new WaitForSeconds(_updateInterval);

            _integerStream.Value += _idleGain; 
        }
    }
    
    private void HandleInteger(int receivedInteger)
    {
        Debug.Log("received integer " + receivedInteger);
        _integerText.text = "Idle Cash: " + receivedInteger;
    }
}
