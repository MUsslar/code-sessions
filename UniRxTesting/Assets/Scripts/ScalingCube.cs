﻿using UniRx;
using UnityEngine;

public class ScalingCube : MonoBehaviour
{
    [SerializeField] private InstantCashButton _instantCashButton;
    [Space]
    [SerializeField] private GameObject _scaledObject;
    [SerializeField] private float _scaleIncrease;

    private void Start()
    {
        var scale = _instantCashButton.Clicks
            .Select(clicks => 1 + clicks * _scaleIncrease)
            .ToReadOnlyReactiveProperty()
            .AddTo(gameObject);

        scale
            .Subscribe(currentScale => _scaledObject.transform.localScale = new Vector3(currentScale, currentScale, currentScale))
            .AddTo(gameObject);
    }
}
