﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class PurchasableUpgrade : MonoBehaviour
{
    [SerializeField] private ReactiveTests _reactiveTests;
    [SerializeField] private Button _upgradeButton;
    [SerializeField] private Image _upgradeButtonImage;
    [SerializeField] private int _clicksToRandomColor;
    [SerializeField] private int _upgradeGain;
    [SerializeField] private int _upgradeCostIncrease;

    private ReactiveProperty<int> _clicks;
    private ReadOnlyReactiveProperty<int> _upgradeCost;
    private ReadOnlyReactiveProperty<bool> _colorRandomizer;

    public IReadOnlyReactiveProperty<int> Clicks
    {
        get { return _clicks; }
    }

    private void Start()
    {
        _clicks = new ReactiveProperty<int>().AddTo(gameObject);

        _upgradeCost = _clicks.Select(clickCount => (clickCount + 1) * _upgradeCostIncrease)
            .ToReadOnlyReactiveProperty()
            .AddTo(gameObject);

        _colorRandomizer = _clicks.Select(clickCount => clickCount % _clicksToRandomColor == 0)
            .ToReadOnlyReactiveProperty()
            .AddTo(gameObject);



        _reactiveTests.IntegerStream.Subscribe(HandleInteger).AddTo(gameObject);

        _colorRandomizer.Skip(1).Subscribe(RandomizeColor).AddTo(gameObject);

        _upgradeButton.OnClickAsObservable()
            .Subscribe(_ => UpdateIdleCash())
            .AddTo(gameObject);
    }

    private void HandleInteger(int receivedInteger)
    {
        _upgradeButton.interactable = receivedInteger >= _upgradeCost.Value;
    }

    private void UpdateIdleCash()
    {
        _reactiveTests.UpdateIdleCash(_upgradeCost.Value, _upgradeGain);

        _clicks.Value += 1;
    }

    private void RandomizeColor(bool randomize)
    {
        if (randomize)
        {
            _upgradeButtonImage.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        }
    }
}
