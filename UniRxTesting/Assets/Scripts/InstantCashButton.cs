﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InstantCashButton : MonoBehaviour
{
    [SerializeField] private ReactiveTests _reactiveTests;
    [SerializeField] private Button _instantCashButton;
    [SerializeField] private int _instantCashValue;

    private ReactiveProperty<int> _clicks;

    public IReadOnlyReactiveProperty<int> Clicks
    {
        get { return _clicks; }
    }

    private void Start()
    {
        _clicks = new ReactiveProperty<int>().AddTo(gameObject);

        _instantCashButton.OnClickAsObservable()
            .Subscribe(_ => OnButtonClick())
            .AddTo(gameObject);
    }

    private void OnButtonClick()
    {
        _clicks.Value++;

        _reactiveTests.IncreaseCash(_instantCashValue);
    }
}
