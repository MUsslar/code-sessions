﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class ListDisplay : MonoBehaviour
{

    private static ListDisplay _instance;

    [SerializeField] private float _updateInterval = 1.0f;
    [Space]
    [SerializeField] private List<ListElement> _elementDisplays;

    private readonly Queue<List<int>> _listQueue = new Queue<List<int>>();
    private readonly Queue<int[]> _arrayQueue = new Queue<int[]>();

    private bool _isDisplaying;

    private int DisplayCount
    {
        get { return _elementDisplays.Count; }
    }

    public static void UpdateDisplays(List<int> list)
    {
        _instance._listQueue.Enqueue(list);

        _instance.StartDisplayingList();
    }

    public static void UpdateDisplays(int[] array)
    {
        _instance._arrayQueue.Enqueue(array);

        _instance.StartDisplayingArray();
    }

    private void StartDisplayingList()
    {
        if (_isDisplaying)
        {
            return;
        }

        _isDisplaying = true;

        StartCoroutine(Display(_listQueue));
    }

    private void StartDisplayingArray()
    {
        if (_isDisplaying)
        {
            return;
        }

        _isDisplaying = true;

        StartCoroutine(Display(_arrayQueue));
    }

    private IEnumerator Display<TCollection>(Queue<TCollection> queue) where TCollection : ICollection, IEnumerable<int>
    {
        var displayCount = _instance.DisplayCount;

        yield return new WaitUntil(() => queue.Count > 0);

        while (queue.Count > 0)
        {
            var displayedCollection = queue.Dequeue();

            var index = 0;
            foreach (var displayedValue in displayedCollection)
            {
                if (index < displayCount)
                {
                    _elementDisplays[index].DisplayedValue = displayedValue;
                    _elementDisplays[index].SetActive(true);

                    index++;
                }
                else
                {
                    break;
                }
            }

            for (var i = index; i < displayCount; i++)
            {
                _elementDisplays[i].SetActive(false);
            }

            yield return new WaitForSeconds(_updateInterval);
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this);
            return;
        }

        _instance = this;
    }
}