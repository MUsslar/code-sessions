﻿using System.Collections.Generic;
using UnityEngine;

public abstract class SortAlgorithm : MonoBehaviour
{

    [SerializeField] private List<int> _unsortedList;

    protected abstract void Sort(List<int> unsortedList);

    private void Start()
    {
        Sort(_unsortedList);
    }
}