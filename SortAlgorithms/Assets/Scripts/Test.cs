﻿using System.Collections.Generic;
using UnityEngine;

// cointainer for functionallity
// should group together things with similar attributes, traits or functionallity
public class Test
{
    // variable made up of
    //  type name
        int integer;

    // everything that you define may get an access modifier
    // such as 'private' (only accessible in the class),
    // 'public' (accessible from everywhere) or
    // 'protected' (accessible from this or inheriting classes)
    // there are other access modifiers, which are of no interest for now
    // if none is give, it will implicitly be given a modifier (in classes by default private)
    private int privateInteger;

    // common types are
    public int Integer;         // whole numbers
    public float Float;         // point value (32-bit)
    public double Double;       // point value (64-bit) (double the amount of float)
    public bool Boolean;        // boolean condition (true or false)
    public char Char;           // a single chracter (number or letter) (is surrounded by '')
    public string String;       // collection of characters (is surrounded by "")

    // variables may be given a default value
    // otherwise they will have their default value
    // (0 (integer), 0.0f (float), 0.0d (double), false (bool), null (string), \x0000 (equivalent to null) (char)
    private int defaultValueInteger = 10;
    
    // types can be put in a collection of the same type
    // either as a List (List<Type>)
    // or as an Array (Type[])
    private List<int> integerList = new List<int>();
    private int[] integerArray = new int[0];
    // lists allow easier use, but will commonly use more memory, as getting new memory is mostly done implicitly

    // properties allow for defining access rights for writing and reading seperatly
    // Properties may also be of any type (exaple uses int)
    private int PrivateInteger { get; set; }
    public int PublicInteger { get; set; }
    public int PublicReadAccessInteger { get; private set; }
    public int PublicWriteAccessInteger { private get; set; }

    // Properties may also define what specificaly is done in their 'get' or 'set' function
    // if no 'get' or 'set' function is defined, it may not be called in the code, as it will throw an ('Not defined') error
    // the 'get' function has to return a value of the property type
    // the 'set' function receives a value of the property type under the name 'value'
    public int DefinedSetInt
    {
        set { value = value + 10; } // only adds 10 to the received value (does not save the received value)
    }

    public int DefinedGetInt
    {
        get { return -100; }
    }
}

public class FunctionTesting : MonoBehaviour
{
    // a function also contains a access modifier, type & name
    // additionally it can have parameters, which have to be passed in, when one wants to call the function
    private void MyFunction(/*int myIntegerParameter, string myStringParameter*/)
    {
        // we can do anything we want here
    }

    // functions may also return values of a type, similar to properties
    private int IntegerFunction()
    {
        int firstValue = 20;
        int secondValue = -10;

        // if the statement is called, we will execute the code in the {}-brackets, otherwise it is skipped
        if (secondValue > 0)
        {
            // this return may be triggered, but as we can not be sure we will need to add a second return statement
            // as a basic rule: all POTENTIAL code paths need to return a value of the function type
            return secondValue;
        }

        // return will exit the function
        // anything in the same line (firstValue + secondValue) is what we return
        return firstValue + secondValue;

        // this is not executed, as we have left the function before
        firstValue = firstValue + 10;
    }

    private void Start()
    {
        // this function is called when the gameobject with this script is first enabled / created

        // each instance of this script will call this function independently from one another
    }

    private void Update()
    {
        // this function is called when the gameobject with this script is enabled
        // it is executed each frame

        // each instance of this script will call this function independently from one another
    }

    private void FixedUpdate()
    {
        // this function is called when the gameobject with this script is enabled
        // it is executed 60 times a second
        // it is executed parallel to the physics update

        // each instance of this script will call this function independently from one another
    }

    private void LateUpdate()
    {
        // this function is called when the gameobject with this script is enabled
        // it is the last thing executed at the end of each frame

        // each instance of this script will call this function independently from one another
    }
}