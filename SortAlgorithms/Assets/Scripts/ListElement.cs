﻿using UnityEngine;
using UnityEngine.UI;

public class ListElement : MonoBehaviour
{

    [SerializeField] private Text _displayedElement;

    public int DisplayedValue
    {
        set { _displayedElement.text = value.ToString(); }
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}