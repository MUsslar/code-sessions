﻿using System;
using System.Collections;
using UnityEngine;

public class Board : MonoBehaviour
{
    [Serializable]
    private class BoardRow
    {
        public TileVisuals[] RowVisuals;
    }

    private const int K_boardHeight = 5;
    private const int K_boardWidth = 7;
    private const int K_tilesNeeded = 2;

    public static Board Instance { get; private set; }

    [SerializeField] private BoardRow[] _visuals;

    private TileVisuals[,] _tileVisuals;
    private TileType[,] _tilesTypes;

    private Vector2 _previousClickedTile = -Vector2.one;

    private Vector2 DefaultPosition
    {
        get { return -Vector2.one; }
    }

    public void OnTileClicked(Vector2 clickedTile)
    {
        if (_previousClickedTile == DefaultPosition)
        {
            _previousClickedTile = clickedTile;
            return;
        }

        if (_previousClickedTile == clickedTile)
        {
            _previousClickedTile = DefaultPosition;
            return;
        }

        TrySwitchTiles(_previousClickedTile, clickedTile);
    }

    private void TrySwitchTiles(Vector2 previousClickedTile, Vector2 clickedTile)
    {
        if (Math.Abs(previousClickedTile.x - clickedTile.x) + Math.Abs(previousClickedTile.y - clickedTile.y) == 1.0f)
        {
            StartCoroutine(SwitchTiles((int)previousClickedTile.x, (int)previousClickedTile.y, (int)clickedTile.x, (int)clickedTile.y));
        }

        _previousClickedTile = DefaultPosition;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Start()
    {
        _tilesTypes = new TileType[K_boardHeight, K_boardWidth];
        _tileVisuals = new TileVisuals[K_boardHeight, K_boardWidth];

        for (var height = 0; height < K_boardHeight; height++)
        {
            for (var width = 0; width < K_boardWidth; width++)
            {
                _tileVisuals[height, width] = _visuals[height].RowVisuals[width];
                _tileVisuals[height, width].SetupTile(height, width);
                UpdateTileType(height, width, TileType.None);
            }
        }

        ResetBoard();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetBoard();
        }
    }

    private void ResetBoard()
    {
        ClearBoard();

        RefillBoard();

        StartCoroutine(UpdateBoard(1));
    }

    private void ClearBoard()
    {
        for (var height = 0; height < K_boardHeight; height++)
        {
            for (var width = 0; width < K_boardWidth; width++)
            {
                UpdateTileType(height, width, TileType.None);
            }
        }
    }

    private void RefillBoard()
    {
        for (var height = 0; height < K_boardHeight; height++)
        {
            for (var width = 0; width < K_boardWidth; width++)
            {
                var type = _tilesTypes[height, width];
                if (type == TileType.None)
                {
                    RandomizeTileType(height, width);
                }
            }
        }
    }

    private IEnumerator SwitchTiles(int previousX, int previousY, int clickedX, int clickedY)
    {
        ChangeBoardInteractivity(false);

        var previousTileType = _tilesTypes[previousX, previousY];
        var clickedTileType = _tilesTypes[clickedX, clickedY];

        UpdateTileType(previousX, previousY, clickedTileType);
        UpdateTileType(clickedX, clickedY, previousTileType);
        
        if (!CheckBoard())
        {
            yield return new WaitForSeconds(0.5f);

            UpdateTileType(previousX, previousY, previousTileType);
            UpdateTileType(clickedX, clickedY, clickedTileType);
        }
        else
        {
            yield return UpdateBoard(0.5f, true);
        }

        ChangeBoardInteractivity(true);
    }

    private IEnumerator UpdateBoard(float initialWait, bool refillBoardImmediate = false)
    {
        ChangeBoardInteractivity(false);

        yield return new WaitForSeconds(initialWait);

        if (refillBoardImmediate)
        {
            DropTileTypes();
            yield return new WaitForSeconds(0.5f);
            RefillBoard();
            yield return new WaitForSeconds(0.5f);
        }
        
        var checkBoard = true;
        while (checkBoard)
        {
            checkBoard = CheckBoard();

            if (checkBoard)
            {
                yield return new WaitForSeconds(0.5f);
                DropTileTypes();
                yield return new WaitForSeconds(0.5f);
                RefillBoard();
                yield return new WaitForSeconds(0.5f);
            }
        }

        ChangeBoardInteractivity(true);
    }

    private void ChangeBoardInteractivity(bool interactive)
    {
        for (var height = 0; height < K_boardHeight; height++)
        {
            for (var width = 0; width < K_boardWidth; width++)
            {
                _tileVisuals[height, width].TileInteractive = interactive;
            }
        }
    }

    private bool CheckBoard()
    {
        var hasChanged = false;

        for (var height = 0; height < K_boardHeight; height++)
        {
            for (var width = 0; width < K_boardWidth; width++)
            {
                var type = _tilesTypes[height, width];
                if (type == TileType.None)
                    continue;

                hasChanged = CheckHorizontal(height, width, type) || hasChanged;
                hasChanged = CheckVertical(height, width, type) || hasChanged;
            }
        }

        return hasChanged;
    }

    private bool CheckHorizontal(int height, int width, TileType type)
    {
        var left = 0;
        var right = 0;
        for (var i = 1; i <= width; i++)
        {
            if (_tilesTypes[height, width - i] == type)
            {
                left += 1;
                // could also be:
                // left++;
            }
            else
            {
                // jumps out of the for loop
                break;
            }
        }
        for (var i = 1; i < K_boardWidth - width; i++)
        {
            if (_tilesTypes[height, width + i] == type)
            {
                right += 1;
                // could also be:
                // right++;
            }
            else
            {
                // jumps out of the for loop
                break;
            }
        }

        if (left + right >= K_tilesNeeded)
        {
            UpdateTileType(height, width, TileType.None);
            for (var i = 1; i <= left; i++)
            {
                UpdateTileType(height, width - i, TileType.None);
            }
            for (var i = 1; i <= right; i++)
            {
                UpdateTileType(height, width + i, TileType.None);
            }

            for (var i = 1; i <= left; i++)
            {
                CheckVertical(height, width - i, type);
            }
            for (var i = 1; i <= right; i++)
            {
                CheckVertical(height, width + i, type);
            }

            return true;
        }

        return false;
    }

    private bool CheckVertical(int height, int width, TileType type)
    {
        var up = 0;
        var down = 0;
        for (var i = 1; i <= height; i++)
        {
            if (_tilesTypes[height - i, width] == type)
            {
                up += 1;
                // could also be:
                // left++;
            }
            else
            {
                // jumps out of the for loop
                break;
            }
        }
        for (var i = 1; i < K_boardHeight - height; i++)
        {
            if (_tilesTypes[height + i, width] == type)
            {
                down += 1;
                // could also be:
                // right++;
            }
            else
            {
                // jumps out of the for loop
                break;
            }
        }

        if (up + down >= K_tilesNeeded)
        {
            UpdateTileType(height, width, TileType.None);
            for (var i = 1; i <= up; i++)
            {
                UpdateTileType(height - i, width, TileType.None);
            }
            for (var i = 1; i <= down; i++)
            {
                UpdateTileType(height + i, width, TileType.None);
            }

            for (var i = 1; i <= up; i++)
            {
                CheckHorizontal(height - i, width, type);
            }
            for (var i = 1; i <= down; i++)
            {
                CheckHorizontal(height + i, width, type);
            }

            return true;
        }

        return false;
    }

    private void DropTileTypes()
    {
        for (var width = 0; width < K_boardWidth; width++)
        {
            DropRow(width);
        }
    }

    private void DropRow(int width)
    {
        var diff = 1;
        for (var height = K_boardHeight - 1; height >= 0; height--)
        {
            if (_tilesTypes[height, width] == TileType.None)
            {
                while (diff <= height)
                {
                    var type = _tilesTypes[height - diff, width];
                    if (type != TileType.None)
                    {
                        UpdateTileType(height, width, type);
                        UpdateTileType(height - diff, width, TileType.None);
                        break;
                    }

                    diff++;
                }
            }
        }
    }

    private void RandomizeTileType(int height, int width)
    {
        _tilesTypes[height, width] = GetRandomTileType();
        _tileVisuals[height, width].SetTileType(_tilesTypes[height, width]);
        //Debug.Log("Randomized Tile: " + height + " ; " + width + " to type " + _tilesTypes[height, width]);
    }

    private void UpdateTileType(int height, int width, TileType type)
    {
        _tilesTypes[height, width] = type;
        _tileVisuals[height, width].SetTileType(type);
        //Debug.Log("Updated Tile: " + height + " ; " + width + " with type: " + type);
    }

    private TileType GetRandomTileType()
    {
        return (TileType) UnityEngine.Random.Range(1, 6);
    }
}