﻿public enum TileType
{
    None,
    Red,
    Green,
    Blue,
    Yellow,
    Purple
}