﻿using UnityEngine;
using UnityEngine.UI;

public class TileVisuals : MonoBehaviour
{

    [SerializeField] private Image _image;
    [SerializeField] private Button _button;

    private Vector2 _tilePosition;

    public bool TileInteractive
    {
        set { _button.interactable = value; }
    }

    public void SetupTile(int height, int width)
    {
        _tilePosition = new Vector2(height, width);
    }
    
    public void SetTileType(TileType type)
    {
        switch (type)
        {
            case TileType.None:
                _image.color = Color.white;
                break;
            case TileType.Red:
                _image.color = Color.red;
                break;
            case TileType.Green:
                _image.color = Color.green;
                break;
            case TileType.Blue:
                _image.color = Color.blue;
                break;
            case TileType.Yellow:
                _image.color = Color.yellow;
                break;
            case TileType.Purple:
                _image.color = Color.magenta;
                break;
            default:
                Debug.LogWarning("No such tile type was defined (" + type + ").");
                _image.color = new Color(0, 0, 0, 0);
                break;
        }
    }

    private void OnButtonClicked()
    {
        Board.Instance.OnTileClicked(_tilePosition);
    }

    private void Awake()
    {
        _button.onClick.AddListener(OnButtonClicked);
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(OnButtonClicked);
    }

    private void OnValidate()
    {
        if (_image != null && _button != null)
            return;

        _image = gameObject.GetComponent<Image>();
        if (_image == null)
        {
            _image = gameObject.AddComponent<Image>();
        }

        _button = gameObject.GetComponent<Button>();
        if (_button == null)
        {
            _button = gameObject.AddComponent<Button>();
        }
    }
}