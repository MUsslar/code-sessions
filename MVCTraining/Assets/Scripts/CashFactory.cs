﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CashFactory : MonoBehaviour
{
    [Serializable]
    private class CashDisplayMapping
    {
        public CashType Type;
        public CashDisplayView View;
    }

    [Serializable]
    private class CashExchangeMapping
    {
        public CashType StartType;
        public CashType ResultType;
        public CurrencyExchangeView View;
    }

    [SerializeField] private CurrencyExchangeConfig _currencyExchangeConfig;
    [Space]
    [SerializeField] private List<CashDisplayMapping> _displayMappings;
    [SerializeField] private List<CashExchangeMapping> _exchangeMappings;

    private void Start()
    {
        var cashModelDictionary = CreateCashModels();

        CreateCashDisplay(cashModelDictionary);

        CreateCashExchange(cashModelDictionary);
    }

    private Dictionary<CashType, CashModel> CreateCashModels()
    {
        Dictionary<CashType, CashModel> cashModelDictionary = new Dictionary<CashType, CashModel>();

        for (int i = 0; i < _displayMappings.Count; i++)
        {
            var mapping = _displayMappings[i];
            var cashModel = new CashModel(mapping.Type).AddTo(gameObject);
            
            cashModelDictionary.Add(mapping.Type, cashModel);
        }

        return cashModelDictionary;
    }

    private void CreateCashDisplay(Dictionary<CashType, CashModel> cashModelDictionary)
    {
        for (int i = 0; i < _displayMappings.Count; i++)
        {
            var mapping = _displayMappings[i];
            var model = cashModelDictionary[mapping.Type];

            // creating display controller
            new CashDisplayController(mapping.View, model, mapping.Type).AddTo(gameObject);
        }
    }

    private void CreateCashExchange(Dictionary<CashType, CashModel> cashModelDictionary)
    {
        for (int i = 0; i < _exchangeMappings.Count; i++)
        {
            var mapping = _exchangeMappings[i];
            var startModel = cashModelDictionary[mapping.StartType];
            var resultModel = cashModelDictionary[mapping.ResultType];

            // creating exchange controller
            new CurrencyExchangeController(mapping.View, _currencyExchangeConfig, startModel, resultModel).AddTo(gameObject);
        }
    }
}