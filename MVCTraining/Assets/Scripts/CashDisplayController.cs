﻿using UniRx;

public class CashDisplayController : UniRxHelper
{
    private readonly CashDisplayView _view;

    public CashDisplayController(CashDisplayView view, CashModel model, CashType type)
    {
        _view = view;

        // set cashIcon depending on cash type
        //view.Icon = null;

        // update cash display depending on reactive property
        model.CurrentCash.Subscribe(DisplayCash).AddTo(Disposer);
    }

    private void DisplayCash(int cash)
    {
        _view.Cash = cash;
    }
}