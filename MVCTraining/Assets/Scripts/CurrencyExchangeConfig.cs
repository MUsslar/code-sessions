﻿using UnityEngine;

[CreateAssetMenu(fileName = "CurrencyExchangeConfig", menuName = "Config Files/Currency Exchange Config")]
public class CurrencyExchangeConfig : ScriptableObject
{
    [SerializeField] private int _currencyExchangeRate;

    public int CurrencyExchangeRate
    {
        get { return _currencyExchangeRate; }
    }
}