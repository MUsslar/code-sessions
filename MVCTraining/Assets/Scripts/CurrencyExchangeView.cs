﻿using UnityEngine;
using UnityEngine.UI;

public class CurrencyExchangeView : MonoBehaviour
{
    [SerializeField] private Image _currentCashIcon;
    [SerializeField] private Text _currentAmountText;
    [SerializeField] private Image _resultCashIcon;
    [SerializeField] private Text _resultAmountText;
    [SerializeField] private Button _exchangeButton;

    public Button ExchangeButton
    {
        get { return _exchangeButton; }
    }

    public Sprite CurrentCash
    {
        set { _currentCashIcon.sprite = value; }
    }
    public Sprite ResultCash
    {
        set { _resultCashIcon.sprite = value; }
    }
    
    public string CurrentAmount
    {
        set { _currentAmountText.text = value; }
    }
    public string ResultAmount
    {
        set { _resultAmountText.text = value; }
    }
}
