﻿using System;
using UniRx;

public class CashModel : UniRxHelper
{
    private const int K_idleCash = 10;
    
    private readonly CashType _type;
    private readonly IReactiveProperty<int> _currentCash;

    public CashType Type
    {
        get { return _type; }
    }

    public IReadOnlyReactiveProperty<int> CurrentCash
    {
        get { return _currentCash; }
    }

    public CashModel(CashType type)
    {
        _type = type;
        
        _currentCash = new ReactiveProperty<int>(0).AddTo(Disposer);
        Observable.Interval(TimeSpan.FromSeconds(1f), Scheduler.MainThread)
            .Subscribe(_ => _currentCash.Value += K_idleCash)
            .AddTo(Disposer);
    }

    public void AddCash(int increase)
    {
        _currentCash.Value += increase;
    }

    public void ReduceCash(int reduction)
    {
        _currentCash.Value -= reduction;
    }
}