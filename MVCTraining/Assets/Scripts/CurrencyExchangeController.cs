﻿using UniRx;

public class CurrencyExchangeController : UniRxHelper
{
    private readonly CurrencyExchangeConfig _config;
    private readonly CashModel _startModel;
    private readonly CashModel _resultModel;

    private readonly int _removedCash;
    private readonly int _addedCash;

    public CurrencyExchangeController(CurrencyExchangeView view,
        CurrencyExchangeConfig config, CashModel startModel, CashModel resultModel)
    {
        _config = config;
        _startModel = startModel;
        _resultModel = resultModel;

        float exchangeRate = GetExchangeRate(startModel.Type, resultModel.Type);
        if (exchangeRate > 1)
        {
            _removedCash = 1;
            _addedCash = (int) exchangeRate;
        }
        else
        {
            _removedCash = (int) (1f / exchangeRate);
            _addedCash = 1;
        }

        view.CurrentAmount = _removedCash.ToString();
        view.ResultAmount = _addedCash.ToString();

        startModel.CurrentCash.Select(cash => cash >= _removedCash)
            .Subscribe(canRemove => view.ExchangeButton.interactable = canRemove)
            .AddTo(Disposer);

        view.ExchangeButton.OnClickAsObservable()
            .Subscribe(click => ExchangeCash())
            .AddTo(Disposer);
    }

    private void ExchangeCash()
    {
        _startModel.ReduceCash(_removedCash);
        _resultModel.AddCash(_addedCash);
    }

    private float GetExchangeRate(CashType start, CashType result)
    {
        switch (start)
        {
            case CashType.Regular:
                switch (result)
                {
                    case CashType.Ice:
                        return 1f / _config.CurrencyExchangeRate;
                    case CashType.Fire:
                        return 1f / _config.CurrencyExchangeRate * _config.CurrencyExchangeRate;
                }

                return 1;
            case CashType.Ice:
                switch (result)
                {
                    case CashType.Regular:
                        return _config.CurrencyExchangeRate;
                    case CashType.Fire:
                        return 1f / _config.CurrencyExchangeRate;
                }

                return 1;
            case CashType.Fire:
                switch (result)
                {
                    case CashType.Regular:
                        return _config.CurrencyExchangeRate * _config.CurrencyExchangeRate;
                    case CashType.Ice:
                        return _config.CurrencyExchangeRate;
                }

                return 1;
        }

        return 1;
    }
}