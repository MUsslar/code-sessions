﻿using System;
using UniRx;

public abstract class UniRxHelper : IDisposable
{
    private readonly CompositeDisposable _disposable = new CompositeDisposable();

    public CompositeDisposable Disposer
    {
        get { return _disposable; }
    }

    public void Dispose()
    {
        _disposable.Dispose();
    }
}