﻿using UnityEngine;
using UnityEngine.UI;

public class CashDisplayView : MonoBehaviour
{

    [SerializeField] private Image _cashIcon;
    [SerializeField] private Text _cashText;

    public Sprite Icon
    {
        set { _cashIcon.sprite = value; }
    }

    public int Cash
    {
        set { _cashText.text = value.ToString(); }
    }
}
